<?php
/**
 * Created by PhpStorm.
 * User: radu.gordan
 * Date: 6/9/2017
 * Time: 01:18 PM
 */

namespace TranslateBundle\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class translateController extends Controller
{
    /**
     * @Route("/{_locale}/translate")
     */
    public function showAction()
    {
        return $this->render('TranslateBundle:Default:translate.html.twig');
    }
}