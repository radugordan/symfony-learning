<?php
/**
 * Created by PhpStorm.
 * User: radu.gordan
 * Date: 6/21/2017
 * Time: 05:34 PM
 */

namespace AppBundle\Command;



use Fadion\Fixerio\Exchange;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class GenerateExchangeHistoricRateCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('app:generate:exchange-history')
            ->setDescription('generate exchange rates trough hisotry')
            ->setHelp('911')
            ->addArgument('from', InputArgument::REQUIRED, 'what do you want to exchange?(RON, EUR, AUD, LVL, USD, etc)' )
            ->addArgument('to',InputArgument::REQUIRED, 'in what do you want to exchange?(RON, EUR, AUD, LVL, USD, etc)')
            ->addOption('date',null, InputOption::VALUE_REQUIRED, 'exchange historically');

    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $xchange = new Exchange();

        $from = $input->getArgument('from');
        $to = $input->getArgument('to');
        $date = $input->getOption('date');
        $xchange->historical($date);
        $xchange->base($from);
        $xchange->symbols($to);
        $var = $xchange->get();
        $output->writeln($var);
    }


}