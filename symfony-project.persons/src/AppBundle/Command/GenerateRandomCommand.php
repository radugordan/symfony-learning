<?php

namespace AppBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class GenerateRandomCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('app:generate:random')
            ->setDescription('generates a random 12 length number')
            ->setHelp('generate random string')
//            ->addArgument('length', InputArgument::OPTIONAL, 'Max length of characters', 12)
            ->addOption('length',
                null,
                InputOption::VALUE_OPTIONAL,
                'Max length of characters',
                12)
//


        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $length = $input->getOption('length');
        $random = uniqid();
        $random = substr($random,0,$length);
        $count = strlen($random);
        $result = 'your random string is: '.$random;
        dump($count);

        $output->writeln($result.'!');
    }

}
