<?php

namespace AppBundle\Service;


use Psr\Log\LoggerInterface;

class LogService
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function logMethod($ourVar)
    {
        $this->logger->debug('Debug first log');
        $this->logger->info('Some info about some action');
        $this->logger->notice('I think it works');
        $this->logger->warning('detailed debug output');
        $this->logger->error('First real error');
        $this->logger->critical('FIRE FIRE ');
        $this->logger->emergency('911');
    
    return true;
    }
}