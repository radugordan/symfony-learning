<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\User;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadUserData implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $userAdmin = new User();
        $userAdmin->setUsername('Nume');
        $userAdmin->setPassword('Parola');
        $userAdmin->setEmail('email@email.ro');

        $manager->persist($userAdmin);
        $manager->flush();

        

    }
}
