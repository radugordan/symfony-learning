<?php
namespace AppBundle\Form;



use AppBundle\Entity\Persons;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PersonsType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname', TextType::class, array(
                'label'=>'translate.firstname', 'attr'=>array(
                    'class'=>"form-control", "css"=>"margin-bottom:15px")))
            ->add('lastname', TextType::class, array(
                'label'=>'translate.lastname', 'attr'=>array(
                    'class'=>"form-control", "css"=>"margin-bottom:15px")))
            ->add('city', TextType::class, array(
                'label'=>'translate.city', 'attr'=>array(
                    'class'=>"form-control", "css"=>"margin-bottom:15px")))
            ->add('adress', TextType::class, array(
                'label'=>'translate.adress', 'attr'=>array(
                    'class'=>"form-control", "css"=>"margin-bottom:15px")))
            ->add('birthdate', DateType::class, array(
                'label'=>'translate.birthdate', 'attr'=>array(
                    'class'=>"formcontrol", "css"=>"margin-bottom:15px")))
            ->add('phone', IntegerType::class, array(
                'label'=>'translate.phone', 'attr'=>array(
                    'class'=>"form-control", "css"=>"margin-bottom:15px")))
            ->add('email', TextType::class, array(
                'label'=>'translate.email', 'attr'=>array(
                    'class'=>"form-control", "css"=>"margin-bottom:15px")))
            ->add('gender', ChoiceType::class, array(
                'label'=>'translate.gender', 'choices'=>array(
                    'translate.male'=>'Male', 'translate.female'=>'Female', 'translate.other'=>'Other')))
            ->add('submit', SubmitType::class, array(
                'label'=>'translate.save', 'attr'=>array(
                    'class'=>'btn btn-primary', 'css'=>'margin-bottom:15px')));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'=>Persons::class,

        ));
    }
}