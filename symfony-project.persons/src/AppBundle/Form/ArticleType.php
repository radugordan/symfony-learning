<?php
/**
 * Created by PhpStorm.
 * User: radu.gordan
 * Date: 6/15/2017
 * Time: 03:38 PM
 */

namespace AppBundle\Form;


use AppBundle\Entity\Articles;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ArticleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, array('attr'=>array(
                "class"=>"form-control", "css"=>"margin-bottom:15px")))
            ->add('content', TextType::class, array('attr'=>array(
                "class"=>"form-control", "css"=>"margin-bottom:15px")))

            ->add('author', EntityType::class, array(
                "class"=>'AppBundle\Entity\Persons',
                "choice_label" => 'firstname'))

            ->add('save', SubmitType::class, array('attr'=>array(
                "class"=>"formcontrol", "css"=>"margin-bottom:15px")));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'=>Articles::class,

        ));
    }

}