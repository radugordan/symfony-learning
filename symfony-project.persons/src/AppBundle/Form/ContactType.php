<?php
/**
 * Created by PhpStorm.
 * User: radu.gordan
 * Date: 6/8/2017
 * Time: 12:20 PM
 */

namespace AppBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;


class ContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add ('from', EmailType::class, array('attr'=>array(
                'class'=>"form-control", "css"=>"margin-bottom:15px")))
            ->add('message', TextareaType::class, array('attr'=>array(
                'class'=>"form-control", "css"=>"margin-bottom:15px")))
            ->add('file', FileType::class, array('attr'=>array(
                'class'=>"formcontrol", "css"=>"margin-bottom:15px")))
            ->add('send', SubmitType::class, array('attr'=>array(
                'class'=>"formcontrol", "css"=>"margin-bottom:15px")))
        ;
    }

}