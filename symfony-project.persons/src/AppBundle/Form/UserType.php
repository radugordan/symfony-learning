<?php
namespace AppBundle\Form;



use AppBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, [
                'label'=> 'translate.email', 'attr'=>array(
                    'class'=>"form-control", "css"=>"margin-bottom:15px")
            ])
            ->add('username', TextType::class, [
                'label'=>'translate.username', 'attr'=>array(
        'class'=>"form-control", "css"=>"margin-bottom:15px")
            ])
            ->add('plainPassword', RepeatedType::class, array(
                'type'=>PasswordType::class,
                'options'=>array('attr'=>array('class'=>"form-control", "css"=>"margin-bottom:15px")),
                'first_options'=>array('label'=>'translate.password'),
                'second_options'=>array('label'=>'translate.repeat_password'),

            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'=>User::class,

        ));
    }
}
//, array('attr'=>array(
//    'class'=>"form-control", "css"=>"margin-bottom:15px"))