<?php

namespace AppBundle\Controller;

    use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
    use AppBundle\Entity\Articles;
    use AppBundle\Entity\Courses;
    use AppBundle\Entity\Images;
    use AppBundle\Entity\Persons;
    use Symfony\Bridge\Doctrine\Form\Type\EntityType;
    use Symfony\Component\Form\Extension\Core\Type\DateType;
    use Symfony\Component\Form\Extension\Core\Type\IntegerType;
    use Symfony\Component\Form\Extension\Core\Type\TextType;
    use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
    use Symfony\Bundle\FrameworkBundle\Controller\Controller;
    use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
    use Symfony\Component\Form\Extension\Core\Type\SubmitType;
    use Symfony\Component\HttpFoundation\Request;


    class PersonsController extends Controller
{
        /**
         * @Route("/", name="list_persons")
         */
        public function listAction()
        {
//            dump($article);

            $this->get('app.example_service')->logMethod('somthing');

//
            $persons = $this->getDoctrine()->getRepository('AppBundle:Persons')
                ->findAll();

            $articles = $this->getDoctrine()->getRepository('AppBundle:Articles')
                ->findAll();

            $em = $this->getDoctrine()->getManager();

            $crs = $em->getRepository(Courses::class)
                ->findAll();

            $students = $em->getRepository(Persons::class)
                ->findAll();



//        dump($students);
//        dump($crs);

            return $this->render(':persons:index.html.twig', array(
                'persons'=>$persons,
                'articles'=>$articles,
                'courses'=>$crs,
                'students'=>$students
            ));
        }


        /**
         * @Route("/add/", name="add_person")
         */
        public function createAction(Request $request)
        {
            $person = new Persons();


            $form = $this->createFormBuilder($person)
                ->add('firstname', TextType::class, array('label'=>'translate.firstname', 'attr'=>array('class'=>"form-control", "css"=>"margin-bottom:15px")))
                ->add('lastname', TextType::class, array('label'=>'translate.lastname', 'attr'=>array('class'=>"form-control", "css"=>"margin-bottom:15px")))
                ->add('city', TextType::class, array('label'=>'translate.city', 'attr'=>array('class'=>"form-control", "css"=>"margin-bottom:15px")))
                ->add('adress', TextType::class, array('label'=>'translate.adress', 'attr'=>array('class'=>"form-control", "css"=>"margin-bottom:15px")))
                ->add('birthdate', DateType::class, array('label'=>'translate.birthdate', 'attr'=>array('class'=>"formcontrol", "css"=>"margin-bottom:15px")))
                ->add('phone', IntegerType::class, array('label'=>'translate.phone', 'attr'=>array('class'=>"form-control", "css"=>"margin-bottom:15px")))
                ->add('email', TextType::class, array('label'=>'translate.email', 'attr'=>array('class'=>"form-control", "css"=>"margin-bottom:15px")))
                ->add('gender', ChoiceType::class, array('label'=>'translate.gender', 'choices'=>array('translate.male'=>'Male', 'translate.female'=>'Female', 'translate.other'=>'Other')))
//                ->add('profile picture', EntityType::class, array(
//                    'class'=>'AppBundle\Entity\Images',
//                    'choice_la'
//                ))
                ->add('submit', SubmitType::class, array('label'=>'translate.save', 'attr'=>array('class'=>'btn btn-primary', 'css'=>'margin-bottom:15px')))

                ->getForm();
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()){

                $person = $form->getData();


                $em = $this->getDoctrine()->getManager();
                $em->persist($person);

                $em->flush();
                $this->addFlash('notice', 'New Person Added');


                return $this->redirectToRoute('list_persons');
            }


            return $this->render(':persons:add.html.twig', array('form'=>$form->createView()));
        }


        /**
         * @Route("/edit/{id}", name="edit_person")
         */
        public function editAction($id, Request $request)
        {
            $person = $this->getDoctrine()
                ->getRepository('AppBundle:Persons')
                ->find($id);
            $person->setFirstname($person->getFirstname());
            $person->setLastname($person->getLastname());
            $person->setAdress($person->getAdress());
            $person->setBirthdate($person->getBirthdate());
            $person->setCity($person->getCity());
            $person->setPhone($person->getPhone());
            $person->setEmail($person->getEmail());
            $person->setGender($person->getGender());

            $form = $this->createFormBuilder($person)
                ->add('firstname', TextType::class, array('label'=>'translate.firstname', 'attr'=>array('class'=>"form-control", "css"=>"margin-bottom:15px")))
                ->add('lastname', TextType::class, array('label'=>'translate.lastname', 'attr'=>array('class'=>"form-control", "css"=>"margin-bottom:15px")))
                ->add('city', TextType::class, array('label'=>'translate.city', 'attr'=>array('class'=>"form-control", "css"=>"margin-bottom:15px")))
                ->add('adress', TextType::class, array('label'=>'translate.adress', 'attr'=>array('class'=>"form-control", "css"=>"margin-bottom:15px")))
                ->add('birthdate', DateType::class, array('label'=>'translate.birthdate', 'attr'=>array('class'=>"formcontrol", "css"=>"margin-bottom:15px")))
                ->add('phone', IntegerType::class, array('label'=>'translate.phone', 'attr'=>array('class'=>"form-control", "css"=>"margin-bottom:15px")))
                ->add('email', TextType::class, array('label'=>'translate.email', 'attr'=>array('class'=>"form-control", "css"=>"margin-bottom:15px")))
                ->add('gender', ChoiceType::class, array('label'=>'translate.gender', 'choices'=>array('translate.male'=>'Male', 'translate.female'=>'Female', 'translate.other'=>'Other')))
                ->add('submit', SubmitType::class, array('label'=>'translate.save', 'attr'=>array('class'=>'btn btn-primary', 'css'=>'margin-bottom:15px')))
                ->getForm();

            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()){
                $firstname = $form['firstname']->getData();
                $lastname = $form['lastname']->getData();
                $adress = $form['adress']->getData();
                $birthdate = $form['birthdate']->getData();
                $city = $form['city']->getData();
                $phone = $form['phone']->getData();
                $email = $form['email']->getData();
                $gender = $form['gender']->getData();

                $em = $this->getDoctrine()->getManager();
                $person = $em->getRepository('AppBundle:Persons')->find($id);

                $person->setFirstname($firstname);
                $person->setLastname($lastname);
                $person->setAdress($adress);
                $person->setBirthdate($birthdate);
                $person->setCity($city);
                $person->setPhone($phone);
                $person->setEmail($email);
                $person->setGender($gender);

                $em->flush();
                $this->addFlash('notice','Person Updated');
                return $this->redirectToRoute('list_persons');
            }


            return $this->render(':persons:edit.html.twig', array(
                'person'=>$person,
                'form'=>$form->createView()
            ));
        }


        /**
         * @Route("/details/{id}", name="detail_person")
         */
        public function detailsAction($id)
        {
            $em = $this->getDoctrine()->getManager();
            $person = $em->getRepository('AppBundle:Persons')->find($id);
            $courses = $person->getCourse();

            $courseRemove = $em->getRepository(Courses::class)->find($id);
//            $courseRemove->removeStudent($person);


            return $this->render(':persons:details.html.twig', array(
                'person'=>$person,
                'courses'=>$courses
            ));
        }

        /**
         * @Route("/{studId}/course/remove/{id}", name="remove_course")
         */
        public function removeCourse($studId, $id)
        {
            $em = $this->getDoctrine()->getManager();
            $course = $em->getRepository(Courses::class)
                ->find($id);
            $student = $em->getRepository(Persons::class)
                ->find($studId);
            $student->removeCourse($course);
            $course->removeStudent($student);
//            dump($course);
//            dump($student);
            $em->flush();
            return $this->redirectToRoute('list_persons');
        }


        /**
         * @Route("/delete/{id}", name="delete_person")
         */
        public function deleteAction($id)
        {
            $em = $this->getDoctrine()->getManager();
            $person = $em->getRepository('AppBundle:Persons')->find($id);

            $em->remove($person);
            $em->flush();
            $this->addFlash('notice','Person Removed');
            return $this->redirectToRoute('list_persons');
        }
}
