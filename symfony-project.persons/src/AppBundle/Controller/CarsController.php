<?php
/**
 * Created by PhpStorm.
 * User: radu.gordan
 * Date: 6/21/2017
 * Time: 07:05 PM
 */

namespace AppBundle\Controller;

use Symfony\Component\Routing\Annotation\Route;
use AppBundle\Entity\Cars;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class CarsController extends Controller
{
    /**
     * @param EntityManager $em
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/show", name="list_cars")
     */
    public function showCarsAction(EntityManager $em)
    {
        $cars = $em->getRepository(Cars::class)
            ->findAll();
        dump($cars);
        return $this->render(':persons:index.html.twig', array(
            'cars'=>$cars
        ));
    }

}