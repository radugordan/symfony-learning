<?php
/**
 * Created by PhpStorm.
 * User: radu.gordan
 * Date: 6/15/2017
 * Time: 03:44 PM
 */

namespace AppBundle\Controller;


use AppBundle\Entity\Articles;
use AppBundle\Form\ArticleType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ArticlesController extends Controller
{
    /**
     * @Route("/article/{id}", name="show_articles", requirements={"id": "\d+"})
     */
    public function detailArticlesAction($id)
    {
        $articles = $this->getDoctrine()
            ->getRepository('AppBundle:Articles')
            ->find($id);
//
//        dump($articles);

        return $this->render('articles/show.html.twig',array(
                "articles"=>$articles
        ));

    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/article/add", name="add_article")
     */
    public function addArticle(Request $request)
    {
        $articles = new Articles();

//        $author = new Persons();

        $form = $this->createForm(ArticleType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){

            $articles = $form->getData();


            $em = $this->getDoctrine()->getManager();
            $em->persist($articles);
            $em->flush();
            return $this->redirectToRoute('list_persons');
        }

        return $this->render(':articles:add.html.twig', array(
            'form' => $form->createView()
        ));

    }

    /**
     * @Route("/article/{id}/edit", name="edit_article")
     */
    public function editArticle($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $article = $em->getRepository(Articles::class)->find($id);
        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $editArticle = $form->getData();
            $em -> flush($editArticle);

            return $this->redirectToRoute('list_persons');

        }

        return $this->render(':articles:edit.html.twig', array(
            'form' => $form->createView()
        ));


    }

    /**
     * @Route("/article/{id}/delete", name="delete_article")
     */
    public function deleteArticle($id)
    {
        $em = $this->getDoctrine()->getManager();
        $article= $em->getRepository(Articles::class)
            ->find($id);
        $em->remove($article);
        $em->flush();
        return $this->redirectToRoute('list_persons');
    }

}