<?php
/**
 * Created by PhpStorm.
 * User: radu.gordan
 * Date: 5/31/2017
 * Time: 09:10 PM
 */

namespace AppBundle\Controller;


use AppBundle\Form\UserType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;


class RegistrationController extends Controller
{
    /**
     * @Route("/register", name="registration")
     */
    public function registerAction(Request $request)
    {

        $request->setLocale('ro');
        
         $this->get('app.example_service')->logMethod('somthing');

        $user = new User();

        $form = $this->createUserRegistrationForm($user);


        return $this->render('registration/register.html.twig', array('form'=>$form->createView()
        ));
    }

    /**
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @Route("/registration-form-submission", name="handle_registration_form_submission")
     * @Method("POST")
     */

    public function handleFormSubmissionAction(Request $request)
    {
        $user = new User();
        $form = $this->createUserRegistrationForm($user);

        $form->handleRequest($request);

        if ( ! $form->isSubmitted() || ! $form->isValid()) {
            return $this->render('registration/register.html.twig', array('form' => $form->createView()
            ));
        }
//        dump('some text');
        $password = $this
            ->get('security.password_encoder')
            ->encodePassword(
                $user,
                $user->getPlainPassword()
            )
        ;
        $user->setPassword($password);

        $em = $this->getDoctrine()->getManager();

        $em->persist($user);
        $em->flush();


        $token = new UsernamePasswordToken(
            $user,
            $password,
            "main",
            $user->getRoles()
        );

        // Remain logged in after register
        $this->get('security.token_storage')->setToken($token);
        $this->get('session')->set('_security_main', serialize($token));
        //

        $this->addFlash('success', 'you are now registerd');

        return $this->redirectToRoute('list_persons');
    }

    /**
     * @param $user
     * @return \Symfony\Component\Form\Form
     */
    private function createUserRegistrationForm($user)
    {
       return $this->createForm(UserType::class, $user, [
            'action' => $this->generateUrl('handle_registration_form_submission')
        ]);
    }
}