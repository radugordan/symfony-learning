<?php
/**
 * Created by PhpStorm.
 * User: radu.gordan
 * Date: 6/16/2017
 * Time: 11:59 AM
 */

namespace AppBundle\Controller;


use AppBundle\Entity\Cars;
use AppBundle\Entity\Courses;
use AppBundle\Entity\Persons;
use Doctrine\ORM\EntityManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class CoursesController extends Controller
{
    /**
     * @Route("/courses", name="list_courses")
     */
    public function listCoursesAction()
    {
        $em = $this->getDoctrine()->getRepository(Cars::class);

        $cars = $em->findAll();
        dump($cars);


        return $this->render('articles/courses.html.twig',array(
                'cars'=>$cars

        ));


    }
}