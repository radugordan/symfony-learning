<?php
/**
 * Created by PhpStorm.
 * User: radu.gordan
 * Date: 6/8/2017
 * Time: 12:36 PM
 */

namespace AppBundle\Controller;


use AppBundle\Form\ContactType;
use Monolog\Logger;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Swift_Message;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Swift_Attachment;

class SupportController extends Controller
{
    /**
     * @param Request $request
     * @Route("/support/", name="support_form")
     */
    public function sendSupportAction(Request $request)


    {
//        $request->setLocale('en');
        $form = $this->createForm(ContactType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){

            $data = $form->getData();

            $file = $form['file']->getData();
            $file->move('file', $file->getClientOriginalName());

            $message = Swift_Message::newInstance()
                ->attach(Swift_Attachment::fromPath('file/'.$file->getClientOriginalName()))
                ->setSubject('Support form request')
                ->setFrom($data['from'])
                ->setTo('radu.gordan@gmail.com')
                ->setBody(
                    $data['message'],
                    'text/plain'
                );
//                dump($_FILES);
            $this->get('mailer')->send($message);

            $transport = $this->container->get('mailer')->getTransport();

            $spool = $transport->getSpool();

            $spool->flushQueue($this->container->get('swiftmailer.transport.real'));


            unlink('file/'.$file->getClientOriginalName());



            $this->addFlash('success', 'Message sent');
            return $this->redirectToRoute('list_persons');
        }

//        $this->get('app.example_service')->logMethod('hello');

        return $this->render('support/support.html.twig', [
            'form'=>$form->createView()
            ]);
    }
}