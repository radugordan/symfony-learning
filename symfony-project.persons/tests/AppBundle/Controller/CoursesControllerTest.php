<?php
namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;



class CoursesControllerTest extends WebTestCase
{
    public function testCourses()
    {
        $client = static::createClient(array(), array(
            'PHP_AUTH_USER' => 'admin',
            'PHP_AUTH_PW'   => 'admin',
        ));

        $client->request('GET', '/courses');
        $response = $client->getResponse();
        dump($response);
//        $crawler = $client->followRedirect();
        $this->assertEquals(200, $response->getStatusCode());

//        $this->assertContains('', $response->getContent());
    }
}