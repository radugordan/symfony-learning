<?php
/**
 * Created by PhpStorm.
 * User: radu.gordan
 * Date: 7/6/2017
 * Time: 07:00 PM
 */

namespace AppBundle\Util;

use Faker\Factory;

class CsvRandomPerson
{
    public function createPerson($txt)
    {
        $fp = fopen($txt, 'w');
        $faker = Factory::create();
        $person = array(array(
            $faker->firstName,
            $faker->lastName,
            $faker->city,
            $faker->streetAddress,
            $faker->date('y-m-d-'),
            $faker->phoneNumber,
            $faker->email,
            $faker->randomElement(['Male', 'Female'])
        ));

        foreach ($person as $line) {
            fputcsv($fp, $line, "|");
        }
        fclose($fp);
    }

}