<?php
namespace AppBundle\Util;




class CharFrequency
{


    public function testText($txt)
    {

        $strArray = count_chars($txt,1);
        arsort($strArray);
        foreach ($strArray as $key=>$value)
        {
            $count[chr($key)] = $value;
        }
        $first_key = key($count);
        return $first_key;
    }

    public function testChars($txt)
    {

        $char = count_chars($txt, 1);
        arsort($char);
        $r = '';
       foreach ($char as $key => $val) {
            $r = $r.chr($key)."-".$val."|" ;
        }
        return $r;



    }
}