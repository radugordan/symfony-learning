<?php
/**
 * Created by PhpStorm.
 * User: radu.gordan
 * Date: 6/21/2017
 * Time: 05:34 PM
 */

namespace AppBundle\Command;




use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

use Ifsnop\Mysqldump as IMysqldump;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Question\Question;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ExportDatabaseCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:db')
            ->setDescription('Exports database schema and/or data')
            ->setHelp('911')
            ->addOption('file-name','',InputOption::VALUE_OPTIONAL, '')
            ->addOption('path','',InputOption::VALUE_OPTIONAL, '')
            ->addOption('only-schema','',InputOption::VALUE_OPTIONAL, '');

    }

    protected function interact(InputInterface $input, OutputInterface $output)
    {
        if (null !== $input->getOption('file-name')) {
            return;
        }

        $output->writeln('');
        $output->writeln('Exports database schema and data');
        $output->writeln('-----------------------------------');

        $output->writeln([
            '',
            'If you prefer to not use this interactive wizard, provide the',
            'options required by this command as follows:',
            '',
            ' $ php bin/console app:db --file-name --path',
            '',
        ]);

        $output->writeln([
            '',
            'Now we\'ll ask you for the value of all the missing command options.',
            '',
        ]);


        $console = $this->getHelper('question');


        $from = $input->getOption('file-name');
        if (null === $from) {
            $question = new Question(' > <info>File-name</info>: ');
            $question->setValidator(function ($answer) {
                if (empty($answer)) {
                    throw new \RuntimeException('You must provide a name for the file');
                }

                return $answer;
            });
            $from = $console->ask($input, $output, $question);
            $input->setOption('file-name', $from);
        } else {
            $output->writeln(' > <info>File-name</info>: '.$from);
        }

        $path = $input->getOption('path');
        if (null === $path) {
            $question = new Question(' > <info>Path</info>: ');
            $question->setValidator(function ($answer) {
                if (empty($answer)) {
                    throw new \RuntimeException('You must provide a path to save the file to');
                }

                return $answer;
            });
            $path = $console->ask($input, $output, $question);
            $input->setOption('path', $path);
        } else {
            $output->writeln(' > <info>Path</info>: '.$path);
        }

    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {


        $host = $this->getContainer()->getParameter('database_host');
        $db = $this->getContainer()->getParameter('database_name');
        $user = $this->getContainer()->getParameter('database_user');
        $pass = $this->getContainer()->getParameter('database_password');

        $path = $input->getOption('path');
        $file = $input->getOption('file-name');


        if (!file_exists($file)) {
            if ($input->hasParameterOption('--only-schema')) {
                try {
                    $dump = new IMysqldump\Mysqldump('mysql:host=' . $host . ';dbname=' . $db , $user, $pass, ['no-data' => true]);

                    $dump->start($path . $file . '.sql');
                    $output->writeln('A dump of your sql database has been created in /home/vagrant/Code/npt_elearn_rad_gor/project_a'.ltrim($path, '.').$file.'.sql');
                } catch (\Exception $e) {
                    echo 'mysqldump-php error: ' . $e->getMessage();
                }
            } else {
                try {
                    $dump = new IMysqldump\Mysqldump('mysql:host='.$host.';dbname='.$db.'', $user, $pass);
//
                    $dump->start($path . $file . '.sql');
                    $output->writeln('A dump of your sql database has been create in /home/vagrant/Code/npt_elearn_rad_gor/project_a'.ltrim($path, '.').$file.'.sql');
                } catch (\Exception $e) {
                    echo 'mysqldump-php error: ' . $e->getMessage();
                }
            }
        } else {
            $output->writeln('A file with that name exists');
        }

//        dump($host, $db, $user, $pass);


        }



}