<?php
/**
 * Created by PhpStorm.
 * User: radu.gordan
 * Date: 7/6/2017
 * Time: 02:57 PM
 */

namespace AppBundle\Command;
use AppBundle\Util\CsvRandomPerson;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Faker\Factory;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Process\Process;

class AddPersonCsvCommand extends ContainerAwareCommand
{
    protected function configure()
    {

        $this
            // a good practice is to use the 'app:' prefix to group all your custom application commands
            ->setName('app:person:generate-csv')
            ->setHelp('911')
            ->setDescription('Generates a csv file containing person data')
            ->addOption('output', null, InputOption::VALUE_REQUIRED, 'The name of the new csv')
            ->addOption('count', null, InputOption::VALUE_OPTIONAL, 'Number of entries to generate', 30);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $person = new CsvRandomPerson();
        $faker = Factory::create();
        $fileName = $input->getOption("output");
        if (empty($fileName)){
           $output->writeln('You need to pass a name for the file (eg. --output filname.csv). See help (eg. --help)');
        } else {
            $fp = fopen($fileName, 'w');

            if ($input->hasOption('count')) {
                for ($i = 0; $i < $input->getOption('count'); $i++) {
                    $person = array(
                        $faker->firstName,
                        $faker->lastName,
                        $faker->city,
                        $faker->streetAddress,
                        $faker->date('y-m-d-'),
                        $faker->phoneNumber,
                        $faker->email,
                        $faker->randomElement(['Male', 'Female'])
                    );

                    fputcsv($fp, $person);

                }fclose($fp);

            } else {
                $person->createPerson($input->getOption('--output'));
            }
        }


    }
}


