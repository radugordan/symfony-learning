<?php
/**
 * Created by PhpStorm.
 * User: radu.gordan
 * Date: 7/6/2017
 * Time: 02:57 PM
 */

namespace AppBundle\Command;
use AppBundle\Util\CsvRandomPerson;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Faker\Factory;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

class SortPersonCsvCommand extends ContainerAwareCommand
{
    protected function configure()
    {

        $this
            // a good practice is to use the 'app:' prefix to group all your custom application commands
            ->setName('app:person:sort-csv')
            ->setHelp('911')
            ->setDescription('Sorts a list of persons by specified column and passes them to a new csv file')
            ->addOption('input', "i", InputOption::VALUE_REQUIRED, 'The name of the imported csv file')
            ->addOption('output', "o", InputOption::VALUE_REQUIRED, 'The name of the new csv')
            ->addOption('sort', "s", InputOption::VALUE_OPTIONAL, 'The type of sorting method (asc/desc)', 'asc')
            ->addOption('column', "c", InputOption::VALUE_REQUIRED, 'By which column (eg. firstname, lastname, city etc) ');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $fileOutput = $input->getOption('output');
        $fileInput = $input->getOption("input");
        $column = $input->getOption("column");
        if (empty($fileInput) || empty($fileOutput) || empty($column)) {
            $output->writeln('You need to pass arguments for all the required values. See --help ');
        } else {
            $output = fopen($input->getOption("output"), 'w');
            $lines = file($fileInput, FILE_IGNORE_NEW_LINES);
            foreach ($lines as $key => $value) {
                $persons[$key] = str_getcsv($value);

            }
            array_walk($persons, function (& $item) {
                $item['firstname'] = $item['0'];
                $item['lastname'] = $item['1'];
                $item['city'] = $item['2'];
                $item['adress'] = $item['3'];
                $item['birthdate'] = $item['4'];
                $item['phone'] = $item['5'];
                $item['email'] = $item['6'];
                $item['gender'] = $item['7'];
                unset($item['0']);
                unset($item['1']);
                unset($item['2']);
                unset($item['3']);
                unset($item['4']);
                unset($item['5']);
                unset($item['6']);
                unset($item['7']);
            });


            global $key;
            $key = $input->getOption("column");


            if ($input->getOption("sort") == "asc") {
                $p = function ($a, $b) {
                    global $key;

                    if ($a[$key] == $b[$key]) {
                        return 0;
                    }
                    return ($a[$key] > $b[$key]) ? 1 : -1;

                };
                uasort($persons, $p);
            } elseif ($input->getOption("sort") == "dsc") {
                $p = function ($a, $b) {
                    global $key;

                    if ($a[$key] == $b[$key]) {
                        return 0;
                    }
                    return ($a[$key] < $b[$key]) ? 1 : -1;
                };
                uasort($persons, $p);
            }

            foreach ($persons as $person) {
                fputcsv($output, $person);
            }
        }
    }
}













