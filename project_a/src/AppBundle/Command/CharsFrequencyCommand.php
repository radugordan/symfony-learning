<?php
/**
 * Created by PhpStorm.
 * User: radu.gordan
 * Date: 7/3/2017
 * Time: 05:16 PM
 */

namespace AppBundle\Command;


use AppBundle\Util\CharFrequency;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\Input;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Validator\Constraints\Optional;

class CharsFrequencyCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:chars')
            ->setDescription('display the frequency of each charachter')
            ->setHelp('911')
            ->addArgument('chars', InputArgument::OPTIONAL, 'feed me')
            ->addOption('file', 'f', InputOption::VALUE_OPTIONAL, 'for loading files in char counter use "-f" flag and give the path to file' );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $char = new CharFrequency();
        if ($input->hasParameterOption('-f')) {
            $fileName = $input->getOption('file');
            $file = fopen($fileName, 'r');
            $chars = fgets($file);


            $result = $char->testChars($chars);
//            dump($result);
            $output->writeln($result);
//
        } else {

            $result = $char->testChars($input->getArgument('chars'));
            $output->writeln($result);
            }
        }

    }
