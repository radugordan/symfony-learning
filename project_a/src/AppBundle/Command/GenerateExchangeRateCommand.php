<?php
/**
 * Created by PhpStorm.
 * User: radu.gordan
 * Date: 6/21/2017
 * Time: 05:34 PM
 */

namespace AppBundle\Command;


use AppBundle\Service\ExchangeService;
use Symfony\Component\Console\Question\Question;
use AppBundle\Service\CurrencyService;
use Fadion\Fixerio\Exchange;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GenerateExchangeRateCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('app:generate:exchange')
            ->setDescription('generate exchange rates trough hisotry')
            ->setHelp('911')
            ->addArgument('from', InputArgument::REQUIRED, 'what do you want to exchange?' )
            ->addArgument('to',InputArgument::REQUIRED, 'in what do you want to exchange?')
            ->addArgument('value',InputArgument::OPTIONAL, 'how much do you want to exchange?');

    }

    protected function interact(InputInterface $input, OutputInterface $output)
    {
        if (null !== $input->getArgument('from') && null !== $input->getArgument('to')&& null !== $input->getArgument('value')) {
            return;
        }

        $output->writeln('');
        $output->writeln('Exchange Rate Interactive Wizard');
        $output->writeln('-----------------------------------');

        $output->writeln([
            '',
            'If you prefer to not use this interactive wizard, provide the',
            'arguments required by this command as follows:',
            '',
            ' $ php bin/console app:generate:exchange from (EUR,RON,AUD) to(USD, LVL, NZD) value',
            '',
        ]);

        $output->writeln([
            '',
            'Now we\'ll ask you for the value of all the missing command arguments.',
            '',
        ]);


        $console = $this->getHelper('question');


        $from = $input->getArgument('from');
        if (null === $from) {
            $question = new Question(' > <info>From</info>: ');
            $question->setValidator(function ($answer) {
                if (empty($answer)) {
                    throw new \RuntimeException('You must provide a currency to exchange from');
                }

                return $answer;
            });
            $from = $console->ask($input, $output, $question);
            $input->setArgument('from', $from);
        } else {
            $output->writeln(' > <info>From</info>: '.$from);
        }


        $to = $input->getArgument('to');
        if (null === $to) {
            $question = new Question(' > <info>To</info>: ');
            $question->setValidator(function ($answer) {
                if (empty($answer)) {
                    throw new \RuntimeException('You must provide a currency to exchange to');
                }

                return $answer;
            });
            $to = $console->ask($input, $output, $question);
            $input->setArgument('to', $to);
        } else {
            $output->writeln(' > <info>To</info>: '.$to);
        }


        $value = $input->getArgument('value');
        if (null === $value) {
            $question = new Question(' > <info>Value</info>: ');
            $question->setValidator(function ($answer) {
                if (empty($answer)) {
                    throw new \RuntimeException('Tell us how much you want to exchange');
                }

                return $answer;
            });
            $value = $console->ask($input, $output, $question);
            $input->setArgument('value', $value);
        } else {
            $output->writeln(' > <info>Value</info>: '.$value);
        }
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $xchange = new ExchangeService();

        $from = $input->getArgument('from');
        $to = $input->getArgument('to');
        $value = $input->getArgument('value');

        $xchange->setBase($from);
        $xchange->setSymbol($to);

        $response = $xchange->exchangeCurrency();


        $result =($response['rates'][$to]* $value);
        $output->writeln($result ."". $to);

    }


}