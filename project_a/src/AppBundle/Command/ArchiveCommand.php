<?php
/**
 * Created by PhpStorm.
 * User: radu.gordan
 * Date: 6/21/2017
 * Time: 05:34 PM
 */

namespace AppBundle\Command;




use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;


use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;



class ArchiveCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:archive')
            ->setDescription('Allows backing up code by creating an archive')
            ->setHelp('911')
            ->addOption('name','',InputOption::VALUE_OPTIONAL, 'Name the archive (eg. myArchive)')
            ->addOption('dest-folder','',InputOption::VALUE_OPTIONAL, 'Choose archive destination(eg. app/Files)');
     }




    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $defaultName = 'archive_'.date('y-m-d_h.i.s');
        $name = $input->getOption('name');
        $dest = $input->getOption('dest-folder');

        if ($input->hasParameterOption('--name') && $input->hasParameterOption('--dest-folder')) {

            $process = new Process('tar cvzf '.$dest.DIRECTORY_SEPARATOR.$name.'.tgz src');
            $process->run();

            if (!$process->isSuccessful()) {
                throw new ProcessFailedException($process);
            }

            echo $process->getOutput();

        }
        else {
            $process = new Process('tar cvzf '.$defaultName.'.tgz src');
            $process->run();

            if (!$process->isSuccessful()) {
                throw new ProcessFailedException($process);
            }

            echo $process->getOutput();
        }



    }



}