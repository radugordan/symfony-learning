<?php
/**
 * Created by PhpStorm.
 * User: radu.gordan
 * Date: 7/3/2017
 * Time: 05:16 PM
 */

namespace AppBundle\Command;


use AppBundle\Util\CharFrequency;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class CharFrequencyCommand extends ContainerAwareCommand
{
    protected function configure()
    {
       $this
           ->setName('app:char')
           ->setDescription('display the character that appears the most ( the char with the highest frequency )')
           ->setHelp('911')
           ->addArgument('char', InputArgument::OPTIONAL, 'feed me')
           ->addOption('file', 'f', InputOption::VALUE_OPTIONAL, 'for loading files in char counter use "-f" flag and give the path to file' );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $char = new CharFrequency();
        if ($input->hasParameterOption('-f')) {
            $fileName = $input->getOption('file');
            $file = fopen($fileName, 'r');
            $chars = fgets($file);


            $result = $char->testText($chars);
            $output->writeln($result);
        } else {

            $result = $char->testText($input->getArgument('char'));
            $output->writeln($result);
        }
    }
}