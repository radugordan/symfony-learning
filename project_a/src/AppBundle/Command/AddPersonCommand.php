<?php



namespace AppBundle\Command;

use Faker\Provider\en_US\Person as Persons;
use AppBundle\Entity\Person;
use Faker\Factory;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Question\Question;


class AddPersonCommand extends ContainerAwareCommand
{
    const MAX_ATTEMPTS = 5;
    private $entityManager;

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {

        $this
            // a good practice is to use the 'app:' prefix to group all your custom application commands
            ->setName('data:person:new')
            ->setDescription('Creates persons and stores them in the database')
//            ->setHelp($this->getCommandHelp())
            ->addOption('firstname', null,InputOption::VALUE_REQUIRED, 'The first name of the new person', '')
            ->addOption('lastname', null,InputOption::VALUE_REQUIRED, 'The last name of the new person')
            ->addOption('gender', null,InputOption::VALUE_REQUIRED, 'The gender of the new person')
            ->addOption('city', null,InputOption::VALUE_REQUIRED, 'The city of the new person')
            ->addOption('adress', null,InputOption::VALUE_OPTIONAL, 'The adress of the new person')
            ->addOption('birthdate', null,InputOption::VALUE_OPTIONAL, 'The birthdate of the new person')
            ->addOption('phone', null,InputOption::VALUE_OPTIONAL, 'The phone number of the new person')
            ->addOption('email', null, InputOption::VALUE_OPTIONAL, 'The email of the new person')
            ->addOption('random', null, InputOption::VALUE_OPTIONAL, 'Generates a new person with random data')
            ->addOption('input-file', null, InputOption::VALUE_OPTIONAL, 'Imports data from file (ex: ./app/Files/person.csv')


        ;
    }


    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->entityManager = $this->getContainer()->get('doctrine')->getManager();
    }




    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $person = new Person();

//        fopen(__DIR__.'../../../app/Files/person.csv', r);


        $faker = Factory::create();

        $rand_firstname = $faker->firstName;
        $rand_lastname = $faker->lastName;
        $rand_gender = $faker->title;
        $rand_city = $faker->city;
        $rand_adress = $faker->address;
        $rand_birthdate = $faker->date();
        $rand_phone = $faker->phoneNumber;
        $rand_email = $faker->email;

        $firstname = $input->getOption('firstname');
        $lastname  = $input->getOption('lastname');
        $gender = $input->getOption('gender');
        $city = $input->getOption('city');
        $adress = $input->getOption('adress');
        $birthdate = $input->getOption('birthdate');
        $phone = $input->getOption('phone');
        $email = $input->getOption('email');



        if ($firstname && $lastname && $city && $gender || $adress || $birthdate || $phone || $email ) {

            $person->setFirstname($input->getOption('firstname', $firstname));
            $person->setLastname($input->getOption('lastname', $lastname));
            $person->setCity($input->getOption('city', $city));
            $person->setAdress($input->getOption('adress', $adress));
            $person->setBirthdate($input->getOption('birthdate', $birthdate));
            $person->setPhone($input->getOption('phone', $phone));
            $person->setEmail($input->getOption('email', $email));
            $person->setGender($input->getOption('gender', $gender));

            $this->entityManager->persist($person);
            $this->entityManager->flush();

            $output->writeln('New random person' ." ".$person->getFirstname() ." ". $person->getLastname() ." ". $person->getGender()." ". $person->getCity());

        }   elseif ($input->hasParameterOption('--random')) {
            $input->setOption('firstname', $rand_firstname);
            $input->setOption('lastname', $rand_lastname);
            $input->setOption('city', $rand_city);
            $input->setOption('adress', $rand_adress);
            $input->setOption('birthdate', $rand_birthdate);
            $input->setOption('phone', $rand_phone);
            $input->setOption('email', $rand_email);
            $input->setOption('gender', $rand_gender);

            $person->setFirstname($input->getOption('firstname'));
            $person->setLastname($input->getOption('lastname'));
            $person->setCity($input->getOption('city'));
            $person->setAdress($input->getOption('adress'));
            $person->setBirthdate($input->getOption('birthdate'));
            $person->setPhone($input->getOption('phone'));
            $person->setEmail($input->getOption('email'));
            $person->setGender($input->getOption('gender'));

//            dump($rand_firstname);

            $this->entityManager->persist($person);
            $this->entityManager->flush();

            $output->writeln('New random person' ." ".$person->getFirstname() ." ". $person->getLastname() ." ". $person->getGender()." ". $person->getCity());

        } elseif($input->hasParameterOption('--input-file')) {

//            $path = __DIR__.'/../../../app/Files/';
            $fileName = $input->getOption('input-file');

            $file = fopen($fileName, 'r');

            while (($row = fgetcsv($file, 0)) !== FALSE) {

            $person = new Person();

            $person->setFirstname($row[0]);
            $person->setLastname($row[1]);
            $person->setCity($row[2]);
            $person->setAdress($row[3]);
            $person->setBirthdate($row[4]);
//            $person->setPhone($row[5]);
            $person->setEmail($row[6]);
            $person->setGender($row[7]);
//            dump($person);

            $this->entityManager->persist($person);
            $this->entityManager->flush();

            $output->writeln('New person from .csv file was created');
            }
        }
        else {
            return;
        }




    }
}
