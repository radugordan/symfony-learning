<?php

namespace AppBundle\Service;


class ExchangeService
{

//    public $url = "http://api.fixer.io/latest";
    private $base;
    private $symbol;
    private $value;


     public function setBase($base)
    {
        $this->base = $base;
        return $this;
    }


    public function setSymbol($symbol)
    {
        $this->symbol = $symbol;
        return $this;
    }
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

    public function exchangeCurrency()
    {
        $url = "http://api.fixer.io/latest";
        $url.= '?base='. $this->base;
        $url.= '&symbols=' . $this->symbol;
        $str = file_get_contents($url);
        $json = json_decode($str, true);
        return $json;


    }

}