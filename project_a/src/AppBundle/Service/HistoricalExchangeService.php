<?php

namespace AppBundle\Service;


class HistoricalExchangeService
{
    private $base;
    private $symbol;
    private $date;

    public function setBase($base)
    {
        $this->base = $base;
        return $this;
    }


    public function setSymbol($symbol)
    {
        $this->symbol = $symbol;
        return $this;
    }
    public function setDate($date)
    {
        $this->date = date('Y-m-d', strtotime($date));
        return $this;
    }


    public function exchangeCurrency()
    {
        $currency = [
            "AUD" => "AUD",
            "BGN" => "BGN",
            "BRL" => "BRL",
            "CAD" => "CAD",
            "CHF" => "CHF",
            "CNY" => "CNY",
            "CZK" => "CZK",
            "DKK" => "DKK",
            "EUR" => "EUR",
            "GBP" => "GBP",
            "HKD" => "HKD",
            "HRK" => "HRK",
            "HUF" => "HUF",
            "IDR" => "IDR",
            "ILS" => "ILS",
            "INR" => "INR",
            "JPY" => "JPY",
            "KRW" => "KRW",
            "MXN" => "MXN",
            "MYR" => "MYR",
            "NOK" => "NOK",
//            "RON" =>"RON",
            "NZD" => "NZD",
            "PHP" => "PHP",
            "PLN" => "PLN",
            "RUB" => "RUB",
            "SEK" => "SEK",
            "SGD" => "SGD",
            "THB" => "THB",
            "USD" => "USD",
            "ZAR" => "ZAR"
            ];
        $now = date('y-m-d');
        $minDate = date('Y-m-d', strtotime("01/01/1999"));
        $maxDate = date('Y-m-d', strtotime($now . '-1 day'));


        if (($this->date >= $minDate) && ($this->date <= $maxDate))
        {
            $url = "http://api.fixer.io/";
            $url.= $this->date;
            $url.= '?base='. $this->base;
            $url.= '&symbols=' . $this->symbol;

            if (!in_array($this->base, $currency)) {
                return 'The base currency was not a valid currency at that time';
            }
            elseif (!in_array($this->symbol, $currency)) {
                return 'The currency you want to convert to was not a valid currency at that time';
            } else {
                $str = file_get_contents($url);
                $json = json_decode($str, true);
                return $json;
            }
        }
        else
        {
            return "Not a valid date. MinDate:" .$minDate .",".  "MaxDate:". $maxDate;
        }

    }



}