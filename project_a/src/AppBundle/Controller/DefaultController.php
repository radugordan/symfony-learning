<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Person;
use AppBundle\Util\CharFrequecy;
use Doctrine\ORM\EntityManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Ifsnop\Mysqldump as IMysqldump;
use ZipArchive;

class DefaultController extends Controller
{


    /**
     * @Route("/", name="homepage")
     */
    public function showAction()
    {

        return $this->render('default/index.html.twig', array());


    }

//
//    /**
//     * @Route("/char/{txt}", name="char")
//     */
//    public function testTesxt($txt)
//    {
//
//        $strArray = count_chars($txt, 1);
//        arsort($strArray);
//
//        $r = '';
//        foreach ($strArray as $key => $val) {
//
//         //$count[chr($key)] = $val;
//         $r.= chr($key).$val;
//
//        }
//
//
//        return $this->render('default/index.html.twig', array(
////            "val" => $count[key($count)],
////            "key"=>chr($key)
//                "count"=>$count
//        ));
//
//    }
}


