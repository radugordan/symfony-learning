<?php

use AppBundle\Command\CharFrequencyCommand;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;
use PHPUnit\Framework\TestCase;

class CharFrequencyCommandTest extends KernelTestCase
{
    public function testChar()
    {

        self::bootKernel();
        $application = new Application(self::$kernel);

        $application->add(new CharFrequencyCommand());

        $command = $application->find('app:char');

        $commandTester = new CommandTester($command);
        $commandTester->execute(array(
            'command'  => $command->getName(),

            // pass arguments to the helper
            'char' => 'a a d d   ',

            // prefix the key with two dashes when passing options,
            // e.g: '--some-option' => 'option_value',
        ));

        // the output of the command in the console
        $output = $commandTester->getDisplay();
        $this->assertContains(' ', $output);

    }

}