<?php

/**
 * Created by PhpStorm.
 * User: radu.gordan
 * Date: 7/5/2017
 * Time: 04:13 PM
 */
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;

class AllCharsFrequencyCommandTest extends KernelTestCase
{
    public function testChars()
    {

        self::bootKernel();
        $application = new Application(self::$kernel);

        $application->add(new \AppBundle\Command\CharsFrequencyCommand());

        $command = $application->find('app:chars');

        $commandTester = new CommandTester($command);
        $commandTester->execute(array(
            'command'  => $command->getName(),

            // pass arguments to the helper
            'chars' => 'ssddf',

            // prefix the key with two dashes when passing options,
            // e.g: '--some-option' => 'option_value',
        ));

        // the output of the command in the console
        $output = $commandTester->getDisplay();
        $this->assertContains('2d2s1f', $output);
    }

}