<?php
///**
// * Created by PhpStorm.
// * User: radu.gordan
// * Date: 7/4/2017
// * Time: 11:17 AM
// */
//
//namespace Tests\AppBundle\Controller;
//
//
//use PHPUnit\Framework\TestCase;
//use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
//
//class test extends TestCase
//{
//    public function testPushAndPop()
//    {
//        $stack = [];
//        $this->assertEquals(0, count($stack));
//
//        array_push($stack, 'foo');
//        $this->assertEquals('foo', $stack[count($stack)-1]);
//        $this->assertEquals(1, count($stack));
//
//        $this->assertEquals('foo', array_pop($stack));
//        $this->assertEquals(0, count($stack));
//    }
//}