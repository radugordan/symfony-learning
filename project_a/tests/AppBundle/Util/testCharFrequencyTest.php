<?php
namespace tests\AppBundle\Util;

use PHPUnit\Framework\TestCase;
use AppBundle\Util\CharFrequency;

class CharFrequencyCommandTest extends TestCase
{
    public function testChars()
    {
        $char = new CharFrequency();
        $result = $char->testText('maamm');
        $this->assertEquals('m', $result);
//        $this->assertEquals('a', $result);

        }

        public function testCharsBlank()
        {
        $char = new CharFrequency();
        $result = $char->testText(' ');
        $this->assertEquals(' ',  $result);

        }

        public function testAllChars()
        {
        $char = new CharFrequency();
        $result = $char->testChars('ttssst');
        $this->assertEquals('3s3t', $result);
    }
}